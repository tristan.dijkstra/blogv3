var languages = ["html.", "css.", "javascript.", "VueJS.", "arduino.", "flask.", "jquery.", "CATIA V5.", "python."]
var interval = 600;

$(document).ready(function () {
  window.setInterval(switcher, 4000);
  i = 0;

  function switcher() {
    // document.getElementById("switchertext").innerHTML = words[i];
    $("#switcher").fadeOut(interval);
    setTimeout(function () {
      document.getElementById("switcher").innerHTML = languages[i];
    }, interval);
    $("#switcher").fadeIn(interval);
    i++;
    if (i >= languages.length) {
      i = 0;
    }
  };

});